package vertxblog.constants;

/**
 * 常量Key类
 */
public class Key {
    public static final String GET_ALL_CATEGORY = "getAllCategory";
    public static final String ADD_CATEGORY = "addCategory";
    public static final String GET_CATEGORY_DETAIL = "getCategoryDetail";
    public static final String UPDATE_CATEGORY = "updateCategory";
    public static final String DELETE_CATEGORY = "deleteCategory";
    public static final String GET_ARTICLE_LIST = "getArticleList";
    public static final String GET_ARTICLE_DETAIL = "getArticleDetail";
    public static final String GET_ARTICLE_ARCHIVE = "getArticleArchive";
    public static final String GET_ARTICLE_ARCHIVE_WITH_CATEID = "getArticleArchiveWithCateid";
    public static final String ADD_ARTICLE = "addArticle";
    public static final String UPDATE_ARTICLE = "updateArticle";
    public static final String GET_ARTICLE_IS_EXIST = "getArticleIsExist";
    public static final String USER_LOGIN = "userLogin";
    public static final String DELETE_ARTICLE = "deleteArt";
}
