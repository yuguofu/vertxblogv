# vertxblog

#### 介绍
java vert.x + mysql博客api

#### 软件架构
java17 + vert.x4.3.1 + mysql8  


#### 安装教程

1.  IDEA打开项目
2.  安装pom.xml依赖
3.  创建数据库、导入sql文件
4.  编辑调试配置： 应用程序 -> 主类 `io.vertx.core.Launcher` -> 运行参数 `run vertxblog.MainVerticle`  
5.  点击调试绿色箭头开始运行  


#### 预览  
![输入图片说明](%E6%88%AA%E5%9B%BE/vertxblog.png)